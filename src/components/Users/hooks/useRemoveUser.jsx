import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useToast } from "@chakra-ui/react";
import { supabase } from "../../../api/api";
import { queryKeys } from "../../../react-query/constants";

async function removeUser(userId) {
  if (userId !== undefined || null) {
    await supabase.from("users").delete().eq("id", userId);
    await supabase.from("addresses").delete().eq("user_id", userId);
  }
}

export function useRemoveUser(userId) {
  const queryClient = useQueryClient();
  const toast = useToast();

  const { mutate } = useMutation({
    mutationFn: () => removeUser(userId),
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: [queryKeys.users] });
      toast({
        title: "User deleted successfully",
        status: "success",
      });
    },
  });

  return mutate;
}
