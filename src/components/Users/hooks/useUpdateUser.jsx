import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useToast } from "@chakra-ui/react";
import { supabase } from "../../../api/api";
import { queryKeys } from "../../../react-query/constants";

async function updateUser(userId, data) {
  await supabase
    .from("users")
    .update([
      {
        firstname: data.firstName,
        lastname: data.lastName,
        email: data.emailAddress,
        birthdate: data.birthDate,
      },
    ])
    .eq("id", userId);

  await supabase
    .from("addresses")
    .update([
      {
        street: data.street,
        city: data.city,
        country: data.country,
        postalcode: data.zipcode,
      },
    ])
    .eq("user_id", userId);
}

export function useUpdateUser(userId) {
  const queryClient = useQueryClient();
  const toast = useToast();

  const { mutate } = useMutation({
    mutationFn: (data) => updateUser(userId, data),
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: [queryKeys.users] });
      toast({
        title: "User updated successfully",
        status: "success",
      });
    },
  });

  return mutate;
}
