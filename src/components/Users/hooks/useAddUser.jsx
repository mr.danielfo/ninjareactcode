import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useToast } from "@chakra-ui/react";
import { supabase } from "../../../api/api";
import { queryKeys } from "../../../react-query/constants";

async function addNewUser(data) {
  await supabase.from("users").insert([
    {
      firstname: data.firstName,
      lastname: data.lastName,
      email: data.emailAddress,
      birthdate: data.birthDate,
    },
  ]);

  // console.log(newUser);

  const fetchUsers = await supabase
    .from("users")
    .select("*")
    .order("id", { ascending: false })
    .limit(1);

  // console.log(fetchUsers);

  await supabase.from("addresses").insert([
    {
      user_id: fetchUsers.data[0].id,
      street: data.street,
      city: data.city,
      country: data.country,
      postalcode: data.zipcode,
    },
  ]);
}

export function useAddUser() {
  const queryClient = useQueryClient();
  const toast = useToast();

  const { mutate } = useMutation({
    mutationFn: addNewUser,
    onSuccess: () => {
      // Invalidate and refetch
      queryClient.invalidateQueries({ queryKey: [queryKeys.users] });
      toast({
        title: "User registered successfully",
        status: "success",
      });
    },
  });

  return mutate;
}
