import { useQuery } from "@tanstack/react-query";
import { supabase } from "../../../api/api";
import { queryKeys } from "../../../react-query/constants";

async function getUserById(id) {
  const fetchUser = await supabase
    .from("users")
    .select("*, addresses!inner(*)")
    .eq("id", id);
  return fetchUser.data[0];
}

export function useGetUsers(userId) {
  const {
    data: user,
    isLoading,
    isError,
  } = useQuery({
    queryKey: [queryKeys.getUser, userId],
    queryFn: () => getUserById(userId),
  });

  return { user, isLoading, isError };
}
