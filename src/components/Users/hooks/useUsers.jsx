import { useQuery } from "@tanstack/react-query";
import { supabase } from "../../../api/api";
import { queryKeys } from "../../../react-query/constants";

async function getUsers() {
  // const fetchQuery = await fetch("../../../../test/users/allusers.json");
  // const dataJson = await fetchQuery.json();
  // return dataJson;
  const fetchUsers = await supabase
    .from("users")
    .select("*, addresses!inner(*)");
  // console.log(fetchUsers);
  return fetchUsers.data;
}

export function useUsers() {
  const {
    data: users,
    isLoading,
    isError,
  } = useQuery({ queryKey: [queryKeys.users], queryFn: getUsers });

  return { users, isLoading, isError };
}
