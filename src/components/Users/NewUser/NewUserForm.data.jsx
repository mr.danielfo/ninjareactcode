import * as Yup from "yup";

const REQUIRED_FIELD = "You need to fill this field";

export const initialValues = {
  firstName: "",
  lastName: "",
  emailAddress: "",
  birthDate: "",
  street: "",
  city: "",
  country: "",
  zipcode: "",
};

export const schema = Yup.object({
  firstName: Yup.string().required(REQUIRED_FIELD).min(2, "hola"),
  lastName: Yup.string().required(REQUIRED_FIELD).min(3),
  emailAddress: Yup.string().email().required(REQUIRED_FIELD),
  birthDate: Yup.string().min(10).max(10).required(REQUIRED_FIELD),
  street: Yup.string().min(3).required(REQUIRED_FIELD),
  city: Yup.string().min(3).required(REQUIRED_FIELD),
  country: Yup.string().min(2).max(2).required(REQUIRED_FIELD),
  zipcode: Yup.string().min(4).max(5).required(REQUIRED_FIELD),
});
