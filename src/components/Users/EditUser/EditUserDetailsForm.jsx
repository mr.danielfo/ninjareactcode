// import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import {
  Button,
  FormControl,
  FormLabel,
  FormErrorMessage,
  Input,
  Select,
} from "@chakra-ui/react";
import { getInitialUserValues, schema } from "./EditUserDetailsForm.data";
import { useUpdateUser } from "../hooks/useUpdateUser";

const EditUserDetailsForm = ({ user, userId }) => {
  const updateUser = useUpdateUser(userId);
  const navigate = useNavigate();
  const {
    register,
    handleSubmit,
    formState: { errors, isDirty },
  } = useForm({
    defaultValues: async () => getInitialUserValues(user),
    resolver: yupResolver(schema),
  });

  const customHandleSubmit = (data) => {
    if (isDirty) {
      updateUser(data);
      navigate("/");
    }
  };

  return (
    <form onSubmit={handleSubmit(customHandleSubmit)}>
      <FormControl isInvalid={errors.firstName}>
        <FormLabel>First name</FormLabel>
        <Input id="firstName" {...register("firstName")} />
        <FormErrorMessage>
          {errors.firstName && errors.firstName?.message}
        </FormErrorMessage>
      </FormControl>
      <FormControl isInvalid={errors.lastName}>
        <FormLabel>Last name</FormLabel>
        <Input id="lastName" {...register("lastName")} />
        <FormErrorMessage>
          {errors.lastName && errors.lastName?.message}
        </FormErrorMessage>
      </FormControl>
      <FormControl isInvalid={errors.emailAddress}>
        <FormLabel>Email address</FormLabel>
        <Input id="emailAddress" {...register("emailAddress")} />
        <FormErrorMessage>
          {errors.emailAddress && errors.emailAddress?.message}
        </FormErrorMessage>
      </FormControl>
      <FormControl isInvalid={errors.birthDate}>
        <FormLabel>Birth date</FormLabel>
        <Input id="birthDate" {...register("birthDate")} />
        <FormErrorMessage>
          {errors.birthDate && errors.birthDate?.message}
        </FormErrorMessage>
      </FormControl>
      <FormControl isInvalid={errors.street}>
        <FormLabel>Street</FormLabel>
        <Input id="street" {...register("street")} />
        <FormErrorMessage>
          {errors.street && errors.street?.message}
        </FormErrorMessage>
      </FormControl>
      <FormControl isInvalid={errors.city}>
        <FormLabel>City</FormLabel>
        <Input id="city" {...register("city")} />
        <FormErrorMessage>
          {errors.city && errors.city?.message}
        </FormErrorMessage>
      </FormControl>
      <FormControl isInvalid={errors.country}>
        <FormLabel>Country</FormLabel>
        <Select placeholder="Select country" {...register("country")}>
          <option value="ES">Spain</option>
          <option value="UK">United Kingdom</option>
          <option value="DE">Denmark</option>
          <option value="US">United States</option>
        </Select>
        <FormErrorMessage>
          {errors.country && errors.country?.message}
        </FormErrorMessage>
      </FormControl>
      <FormControl isInvalid={errors.zipcode}>
        <FormLabel>Zipcode</FormLabel>
        <Input id="zipcode" {...register("zipcode")} />
        <FormErrorMessage>
          {errors.zipcode && errors.zipcode?.message}
        </FormErrorMessage>
      </FormControl>
      <Button
        mt={4}
        colorScheme="blue"
        isLoading={false}
        type="submit"
        loadingText="Submitting"
      >
        Submit edit changes
      </Button>
      <Link to="/">
        <Button mt={4} ml={4} colorScheme="yellow">
          Cancel
        </Button>
      </Link>
    </form>
  );
};

export default EditUserDetailsForm;
