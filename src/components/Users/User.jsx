import { Link } from "react-router-dom";
import {
  Tr,
  Td,
  Text,
  Button,
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverHeader,
  PopoverBody,
  PopoverCloseButton,
  PopoverArrow,
  useDisclosure,
} from "@chakra-ui/react";

import { useRemoveUser } from "./hooks/useRemoveUser";

const User = ({ userId, firstName, lastName, email, birthDate }) => {
  const { onOpen } = useDisclosure();
  const removeUser = useRemoveUser(userId);
  const handleDeleteUser = () => {
    removeUser();
  };

  return (
    <Tr>
      <Td>{firstName}</Td>
      <Td>{lastName}</Td>
      <Td>{email}</Td>
      <Td>{birthDate}</Td>
      <Td>
        <Link to={`/users/${userId}`}>
          <Button colorScheme="yellow">Edit</Button>
        </Link>
      </Td>
      <Td>
        <Popover placement="right" closeOnBlur={true}>
          <PopoverTrigger>
            <Button onClick={onOpen} colorScheme="red">
              Delete
            </Button>
          </PopoverTrigger>
          <PopoverContent>
            <PopoverArrow />
            <PopoverCloseButton />
            <PopoverHeader pt={6}>
              Do you want to delete <Text as="b">{firstName}</Text>?
            </PopoverHeader>
            <PopoverBody>
              <Button onClick={handleDeleteUser}>Yes</Button>
            </PopoverBody>
          </PopoverContent>
        </Popover>
      </Td>
    </Tr>
  );
};

export default User;
