import { Tr, Th } from "@chakra-ui/react";

const ColumnNames = () => {
  return (
    <Tr>
      <Th>First name</Th>
      <Th>Last name</Th>
      <Th>email</Th>
      <Th>Birth date</Th>
      <Th>Edit</Th>
      <Th>Delete</Th>
    </Tr>
  );
};

export default ColumnNames;
