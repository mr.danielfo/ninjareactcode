import { Container, Text } from "@chakra-ui/react";
import NewUserForm from "../../components/Users/NewUser/NewUserForm";

const RegisterUser = () => {
  return (
    <Container maxW="container.lg">
      <Text fontSize="2xl" mb={4}>
        Register New User
      </Text>
      <NewUserForm />
    </Container>
  );
};

export default RegisterUser;
