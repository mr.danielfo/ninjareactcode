import { Link } from "react-router-dom";
import {
  Table,
  Thead,
  Tbody,
  TableContainer,
  Tr,
  Td,
  Text,
  Box,
  Button,
  Skeleton,
} from "@chakra-ui/react";
import ColumnNames from "../../components/Users/ColumnNames";
import User from "../../components/Users/User";
import { useUsers } from "../../components/Users/hooks/useUsers";

const ShowUsers = () => {
  const { users, isLoading, isError } = useUsers();

  if (isLoading || isError)
    return (
      <Box>
        <Skeleton>
          <div data-testid="loading">contents wrapped</div>
          <div>won't be visible</div>
        </Skeleton>
      </Box>
    );

  return (
    <Box w="100%" p={3}>
      <Text fontSize="2xl" data-testid="title">
        Users
      </Text>
      <TableContainer>
        <Table variant="simple">
          <Thead>
            <ColumnNames />
          </Thead>
          <Tbody>
            {users.length === 0 ? (
              <Tr>
                <Td>Add some users</Td>
              </Tr>
            ) : (
              users.map((user) => (
                <User
                  key={user.id}
                  userId={user.id}
                  firstName={user.firstname}
                  lastName={user.lastname}
                  email={user.email}
                  birthDate={user.birthdate}
                />
              ))
            )}
          </Tbody>
        </Table>
      </TableContainer>
      <Link to="/register-user">
        <Button mt={4} ml={2} colorScheme="blue">
          Register new user
        </Button>
      </Link>
    </Box>
  );
};

export default ShowUsers;
