import { useParams } from "react-router-dom";
import { Container, Text, Skeleton, Box } from "@chakra-ui/react";
import EditUserDetailsForm from "../../components/Users/EditUser/EditUserDetailsForm";
import { useGetUsers } from "../../components/Users/hooks/useGetUsers";

const EditUser = () => {
  const { id } = useParams();
  const { user, isLoading, isError } = useGetUsers(id);

  if (isLoading || isError)
    return (
      <Box>
        <Skeleton>
          <div>contents wrapped</div>
          <div>won't be visible</div>
        </Skeleton>
      </Box>
    );

  return (
    <Container maxW="container.lg">
      <Text fontSize="2xl" mb={4}>
        User Details
      </Text>
      <EditUserDetailsForm user={user} userId={id} />
    </Container>
  );
};

export default EditUser;
