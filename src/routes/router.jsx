import { createBrowserRouter } from "react-router-dom";
import EditUser from "../pages/Users/EditUser";
import RegisterUser from "../pages/Users/RegisterUser";
import Root from "./root";

export const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
  },
  {
    path: "/users/:id",
    element: <EditUser />,
  },
  {
    path: "/register-user",
    element: <RegisterUser />,
  },
]);
