import { createClient } from "@supabase/supabase-js";

import { supabaseUrl, supabaseKey } from "./constants";

export const supabase = createClient(supabaseUrl, supabaseKey);
