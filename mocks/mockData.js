/** USER  ************************************************************************* */
export const mockAddUser = {
  id: 4,
  firstname: "Pepe",
  lastname: "Panda",
  email: "pepe.panda@example.com",
  birthDate: "1990-03-23",
  address: {
    id: 1,
    street: "Lindenstraße 89",
    city: "Freiburg im Breisgau",
    country: "DE",
    postalcode: "42030",
  },
};

export const mockEditUser = {
  id: 1,
  firstname: "Valia",
  lastname: "Ojeda Smith",
  email: "valia.smith@example.com",
  birthDate: "1984-01-23",
  address: {
    id: 1,
    street: "Lindenstraße 89",
    city: "Freiburg im Breisgau",
    country: "DE",
    postalcode: "42030",
  },
};

export const mockUsers = [
  {
    id: 1,
    firstname: "John",
    lastname: "Smith",
    email: "john.smith@example.com",
    birthDate: "1980-01-23",
    address: {
      id: 1,
      street: "Lindenstraße 89",
      city: "Freiburg im Breisgau",
      country: "DE",
      postalcode: "42030",
    },
  },
  {
    id: 2,
    firstname: "Jane",
    lastname: "Malcovich",
    email: "jane.malcovich@example.com",
    birthDate: "1981-02-24",
    address: {
      id: 2,
      street: "Lindenstraße 86",
      city: "Frankfurt",
      country: "DE",
      postalcode: "41330",
    },
  },
  {
    id: 3,
    firstname: "Samrat",
    lastname: "Sen",
    email: "samrat.sen@example.com",
    birthDate: "1969-04-18",
    address: {
      id: 3,
      street: "Indira Nehru",
      city: "Hyderabad",
      country: "IN",
      postalcode: "50047",
    },
  },
];
