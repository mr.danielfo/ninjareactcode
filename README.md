# React Ninja Code Challenge

## This project is powered by Vite

### Steps for setting this project in your local machine

- Clone this repository 
- Inside ninjareactcode directory run npm install
- Later execute npm run dev
- For checking tests execute npm run test

### This project uses Supabase as third party library for storing data and fetching data