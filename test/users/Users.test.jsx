import { mockUsers, mockAddUser, mockEditUser } from "../../mocks/mockData";

test("total users must be equal to 3", async () => {
  expect(mockUsers.length).toBe(3);
});

test("firstname of user with id 1 must be John ", async () => {
  const firstname = "John";
  const user = mockUsers.find((user) => user.id === 1);
  expect(user).toBeTruthy();
  expect(user.firstname).toBe(firstname);
});

test("add user ", async () => {
  const newUsers = [...mockUsers, mockAddUser];
  expect(newUsers.length).toBe(4);
});

test("remove user with id 3 ", async () => {
  const users = mockUsers.filter((user) => user.id !== 3);
  const withOutId = users.filter((user) => user.id === 3);
  expect(users.length).toBe(2);
  expect(withOutId.length).toBe(0);
});

test("edit user with id 1 and firstname must be Valia", async () => {
  let user = mockUsers.find((user) => user.id === 1);
  user = mockEditUser;
  expect(user.firstname).toBe("Valia");
});
